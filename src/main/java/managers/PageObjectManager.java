package managers;

import PageObjectModule.SnapHub;
import org.openqa.selenium.WebDriver;


public class PageObjectManager {


    private WebDriver driver;
    private SnapHub snapHubVerify;
   

    public PageObjectManager(WebDriver driver) {

        this.driver = driver;

    }
    public SnapHub login(){

        return (snapHubVerify == null) ? snapHubVerify = new SnapHub( driver ) : snapHubVerify;

    }


}
