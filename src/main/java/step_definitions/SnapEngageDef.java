package step_definitions;

import cucumber.api.java8.En;
import org.openqa.selenium.WebDriver;

import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;
import PageObjectModule.SnapHub;
import managers.PageObjectManager;
public class SnapEngageDef implements En {
    WebDriver driver;
    SnapHub loginPage;
    PageObjectManager pageObjectManager;
    
    String UID;
    String PWD;
    String URL;
    public SnapEngageDef() throws IOException {
        FileReader reader=new FileReader("src/main/java/PageObjectModule/property.properties");
        Properties read=new Properties();
        read.load(reader);
        UID=read.getProperty("userName");
        PWD=read.getProperty("password");
        URL=read.getProperty("url");


        Given( "^login to the application$", () -> {
            pageObjectManager = new PageObjectManager( driver );
            loginPage=pageObjectManager.login();
            loginPage.loginPage( UID,PWD,URL );

        } );
        When( "^Assert that the hub is loaded$", () -> {

            loginPage.verifyHomePage();

        } );
        Then( "^close the browser$", () -> {
             loginPage.logout();

        } );
    }
}
