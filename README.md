# Adjust Test Automation script



* In this code I have used Cucumber BDD concept, so it should start from Runner.class
* There is Feature file and step_Defintion file
* Data is driven from property file
* PageObjectModule is implemented 
* There is Page Object Manager created to avoid creating object for each Page object class again and again. 
* The screenshot is added under target folder --> screenShots
* Assertion is done for a element which will be displayed onle once the hub is launched.
* Detail Report will be found in target -->cucumber 


